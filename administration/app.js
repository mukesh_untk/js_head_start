class Place {
    constructor (name, buildYear) {
        this.name = name;
        this.buildYear = buildYear;
    }
}

class Park extends Place {
    constructor (name, buildYear, area, treeCount) {
        super(name, buildYear);
        this.area = area;
        this.treeCount = treeCount;
    }

    treeDensity() {
        console.log(`${this.name} has a tree density of ${this.treeCount/this.area} trees per square km`);
    }
}

class Street extends Place {
    constructor (name, buildYear, length, size=3) {
        super(name, buildYear);
        this.length = length;
        this.size = size;
    }

    showDetails () {
        console.log(`${this.name}, bulit in ${this.buildYear}, is a ${this.streetSize()} street.`);
    }

    streetSize() {
        switch (this.size) {
            case 1 :
                return 'tiny';
            case 2 :
                return 'small';
            case 3 :
                return 'normal';
            case 4 :
                return 'big';
            case 5 : 
                return 'huge';
            default :
                return 'normal';
        }
    }
}


function initParks() {
    const greenPark = new Park('Green Park', 1987, 0.2, 215);
    const nationalPark = new Park('National Park', 1894, 2.9, 3541);
    const oakPark = new Park('Oak Park', 1953, 0.4, 949);

    return [greenPark, nationalPark, oakPark];
}

function initStreets() {
    return [new Street('Ocean Avenue', 1999, 1.1, 4),
            new Street('Evergreen Street', 2008, 2.7, 2),
            new Street('4th Street', 2015, 0.8),
            new Street('Sunset Boulevard', 1982, 2.5, 5)];
}

function showParksReport() {
    const parks = initParks();
    console.log('\n------PARKS REPORT------')
    const count = parks.length;
    let average = 0;
    parks.forEach(cur => average += new Date().getFullYear() - cur.buildYear);
    console.log(`our ${count} parks have an average age of ${average/count} years.`)
    parks.forEach((cur) => cur.treeDensity());
    const p = parks.find(cur => {
        return cur.treeCount > 1000
    })
    console.log(`${p.name} has more than 1000 trees.`);
}

function showStreetsReport() {
    const streets = initStreets();
    console.log('\n------STREETS REPORT------')
    let total = 0;
    streets.forEach( s => total += s.length);
    console.log(`our ${streets.length} streets have a total length of ${total} km, with an average of ${total/streets.length} km. `)
    streets.forEach(cur => cur.showDetails());
}

function showReport() {
    showParksReport();
    showStreetsReport();
}

showReport();