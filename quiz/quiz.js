var Question = function(question, answers, correctAnswer) {
    this.question = question,
    this.answers = answers,
    this.correctAnswer = correctAnswer
}

function getQuestion() {
    var q1 = new Question("Javascript is _________ language.",
        ["None of These",
        "Application",
        "Programming",
        "Scripting"], 3);

    var q2 = new Question("JavaScript is ______ Side Scripting Language.",
        ["ISP",
        "None of These",
        "Browser",
        "Server"
        ], 2);

    var q3 = new Question("JavaScript is designed for following purpose -",
        ["To Perform Server Side Scripting Opertion",
        "To Execute Query Related to DB on Server",
        "To add interactivity to HTML Pages.",
        "To Style HTML Pages"
        ], 2);

    var q4 = new Question("JavaScript is can be written -",
        ["directly into HTML pages",
        "All",
        "directly into JS file and included into HTML"
        ], 1);

    var q5 = new Question("JavaScript is an ________ language.",
        ["interpreted",
        "compiled"
        ], 0);

    var q6 = new Question("Cost for Using JavaScript in your HTML is _________ .",
        ["Its Free !!!",
        "$5 / Year",
        "$10 / Year",
        "$15 / Year"
        ], 0);

    var q7 = new Question("JavaScript Code is written inside file having extension __________.",
        [".js",
        ".jvs",
        ".jsc",
        ".javascript"
        ], 0);


    var q8 = new Question("Why JavaScript is called as Lightweight Programming Language ?",
        ["because JS is available free of cost.",
        "because we can add programming functionality inside JS",
        "because JS can provide programming functionality inside but up to certain extend.",
        "because JS is client side scripting"
        ], 2);


    var q9 = new Question("Choose appropriate Option(s) : JavaScript is also called as _____________.",
        ["None of These",
        "Browser Side Scripting Language",
        "Server Side Scripting Language",
        "Client Side Scripting Language"
        ], 3);


    var q10 = new Question("Local Browser used for validations on the Web Pages uses __________.",
        ["Java",
        "JS",
        "HTML",
        "CSS"], 1);

    return [q1, q2, q3, q4, q5, q6, q7, q8, q9, q10];
}
var questions = getQuestion();
var score;

window.onload = function() {
    this.score = 0;
    nextQuestion();
}

function nextQuestion() {
    (function(){
        var q = Math.round(Math.random()*10);
        showQuestion(questions[q]);
    })();
}

function showQuestion(ques) {
    console.log(ques.question);
    for(var i=0; i< ques.answers.length;i++){
        console.log(i, ques.answers[i])
    }
    var ans = prompt(ques.question);
    if(ans == 'exit') return;
    if (ans == ques.correctAnswer) {
        console.log("Correct Answer!");
        score += 10;   
    }  
    
    console.log("==========================");
    console.log("SCORE : ", score);
    console.log("==========================");
    nextQuestion();
}


