window.onload = function() {
    this.init();
}

var scores, roundScore, player, isGameOn;
player = 0;

function init() {
    // player = 0;
    scores = [0, 0];
    roundScore = 0;
    isGameOn = true;
    document.querySelector('#score-0').textContent = '0';
    document.querySelector('#score-1').textContent = '0';
    document.querySelector('#current-0').textContent = '0';
    document.querySelector('#current-1').textContent = '0';
    document.querySelector('.btn-roll').addEventListener('click', rollDice);
    document.querySelector('.btn-new').addEventListener('click', init);
    document.querySelector('.btn-hold').addEventListener('click', hold);
    document.querySelector('.player-0-panel').classList.remove('active');
    document.querySelector('.player-1-panel').classList.remove('active');
    document.querySelector('.player-0-panel').classList.remove('winner');
    document.querySelector('.player-1-panel').classList.remove('winner');
    document.querySelector('.player-' + player + '-panel').classList.add('active');
    document.getElementById('name-0').textContent = prompt('Player 1\'s Name'); //'Player 1';
    document.getElementById('name-1').textContent = prompt('Player 2\s Name'); //'Player 2';
}

function rollDice() {

    if (!isGameOn) return;
    var dice = Math.floor(Math.random() * 6 + 1 );
    document.querySelector('.dice').src = 'dice-' + dice + '.png';
    roundScore += dice;
    if (dice == 1) roundScore = 0;
    document.querySelector('#current-' + player).textContent = roundScore;
    if(!roundScore) hold();
}

function hold() {

    if (!isGameOn) return;

    scores[player] += roundScore;

    if(scores[player] >= 100) {
        document.querySelector('.player-' + player + '-panel').classList.add('winner');
        document.getElementById('name-' + player).textContent = 'WINNER!';
        isGameOn = false
    }

    roundScore = 0;
    document.querySelector('#score-' + player).textContent = scores[player];
    document.querySelector('#current-' + player).textContent = 0;
    var panel = document.querySelector('.player-' + player + '-panel');
    panel.classList.remove('active');
    player = player == 0 ? 1 : 0;
    panel = document.querySelector('.player-' + player + '-panel');
    panel.classList.add('active');
}