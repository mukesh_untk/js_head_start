
import Search from './model/Search.js';
import Recipe from './model/Recipe.js';
import { elements } from './view/base.js';
import { getInput, clearInput, renderResults } from './view/searchView.js';
import { clearRecipe, randerRecipe, randerIngredients, updateServings } from './view/recipeView.js'
import * as listview from './view/listView.js';


const state = {}

const controlledSearch = () => {
    const query = getInput();
    state.search = new Search(query);
}

const contorlRecipe = async () => {
    const id = window.location.hash.replace('#', '');
    state.recipe = new Recipe(id);
    
    await state.recipe.getRecipe();

    // console.log(recipe);
    clearRecipe();
    randerRecipe(state.recipe);
}

const searchQuery = async (offset=0) => {
    await state.search.getResults(offset);    
    renderResults(state.search.result);
    clearInput();
}

document.querySelector(elements.search).addEventListener('submit', (e) => {
    e.preventDefault();
    controlledSearch();
    searchQuery();
});

document.querySelector('.results__pages').addEventListener('click', (e)=> {
    const btn = e.target.closest('.btn-inline');
    if(btn) {
        const page = parseInt(btn.dataset.page);
        searchQuery(page);
    }
});

window.addEventListener('hashchange', contorlRecipe);

document.querySelector('.recipe').addEventListener('click', e=>{
    if(e.target.matches('.recipe__btn *')){
        document.querySelector('.shopping__list').innerHTML = '';
        state.recipe.Ingredients.forEach(item=> {
            listview.randerItem(item);
        });
    }

    if(e.target.matches('.btn-decrease, .btn-decrease *')){
        if(state.recipe.servings > 1) {
            const newserving = state.recipe.servings - 1;
            state.recipe.Ingredients.forEach(ing=>{               
                ing.amount = (ing.amount * (newserving/state.recipe.servings)).toFixed(2);
            });
            state.recipe.servings = newserving;
            randerIngredients(state.recipe.Ingredients);
            updateServings(newserving);
        }       
    }else if (e.target.matches('.btn-increase, .btn-increase *')) {
        const newserving = state.recipe.servings + 1;
        state.recipe.Ingredients.forEach(ing=>{
            ing.amount = (ing.amount * (newserving/state.recipe.servings)).toFixed(2);
        });
        state.recipe.servings = newserving;
        randerIngredients(state.recipe.Ingredients);   
        updateServings(newserving);
    }  
});

// document.querySelector('').addEventListener('click', ()=> {

// });