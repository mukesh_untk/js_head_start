import {elements} from './base.js';

export const getInput = () => document.querySelector(elements.input).value;
export const clearInput = () => {document.querySelector(elements.input).value= ''};
const clearResults = () => {
    document.querySelector('.results__pages').innerHTML = '';
    document.querySelector('.results__list').innerHTML = '';
};

function renderRecipe(recipe) {
    const imgURL = 'https://spoonacular.com/recipeImages/';
    const item = `
        <li>
            <a class="results__link" href="#${recipe.id}">
                <figure class="results__fig">
                    <img src="${imgURL+recipe.image}" alt="${recipe.title}">
                </figure>
                <div class="results__data">
                    <h4 class="results__name">${recipe.title}</h4>
                    <p class="results__author">${recipe.readyInMinutes} Minutes | ${recipe.servings} Servings.</p>
                </div>
            </a>
        </li>`;        
    document.querySelector('.results__list').insertAdjacentHTML('afterbegin', item);
}

const getPagination = (btn, cur) => {
    const elem = `<button class="btn-inline results__btn--${btn}" data-page="${ btn == 'prev' ? cur-1 : cur+1 }">
                        <svg class="search__icon">
                            <use href="img/icons.svg#icon-triangle-${ btn == 'prev' ? 'left' : 'right' }"></use>
                        </svg>
                        <span>Page ${ btn == 'prev' ? cur : cur+2 }</span>
                    </button>`;
    
    document.querySelector('.results__pages').insertAdjacentHTML('afterbegin', elem);
}


export const renderResults = (result) => {
    clearResults();

    result.results.forEach(el => {
        renderRecipe(el);
    });

    // prev next 
    if(result.offset === 0 && result.totalResults > result.number) {
        getPagination('next', result.offset);
    }
    else if (result.totalResults <= (result.number * result.offset)) {
        getPagination('prev', result.offset);
    }
    else {
        getPagination('next', result.offset);
        getPagination('prev', result.offset);
    }
}