
export const randerItem = (item) => {
    const markup = `
    <li class="shopping__item item_${item.id}">
        <div class="shopping__count">
            <input type="number" value="${item.amount}" step="${item.amount}">
            <p>${item.unit}</p>
        </div>
        <p class="shopping__description">${item.name}</p>
        <button class="shopping__delete btn-tiny">
            <svg>
                <use href="img/icons.svg#icon-circle-with-cross"></use>
            </svg>
        </button>
    </li>
    `;
    document.querySelector('.shopping__list').insertAdjacentHTML('beforeend', markup);
};


export const deleteItem = (id) => {
    const item = document.querySelector('.shopping__item_'+id);
    if(item) item.parentElement.removeChild(item);
};