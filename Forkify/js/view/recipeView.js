

export const clearRecipe = () => document.querySelector('.recipe').innerHTML = '';
export const updateServings = (c) => document.querySelector('.recipe__info-data--people').innerHTML = c;
export const randerIngredients = (ingredients) => {
    let markup = '';    
    document.querySelector('.recipe__ingredient-list').innerHTML = markup;
    ingredients.forEach(ingredient => { 
        markup += `<li class="recipe__item">
            <svg class="recipe__icon">
                <use href="img/icons.svg#icon-check"></use>
            </svg>
            <div class="recipe__count">${ ingredient.amount }</div>
            <div class="recipe__ingredient">
                <span class="recipe__unit">${ ingredient.unit }</span>
                ${ ingredient.name }
            </div>
        </li>`;
    });
    document.querySelector('.recipe__ingredient-list').insertAdjacentHTML('afterbegin', markup);
};

export const randerRecipe = (recipe)=> {
    const template = `
        <figure class="recipe__fig">
        <img src="${recipe.image}" alt="Tomato" class="recipe__img">
        <h1 class="recipe__title">
            <span>${recipe.title}</span>
        </h1>
        </figure>
        <div class="recipe__details">
            <div class="recipe__info">
                <svg class="recipe__info-icon">
                    <use href="img/icons.svg#icon-stopwatch"></use>
                </svg>
                <span class="recipe__info-data recipe__info-data--minutes">${ recipe.readyInMinutes }</span>
                <span class="recipe__info-text"> minutes</span>
            </div>
            <div class="recipe__info">
                <svg class="recipe__info-icon">
                    <use href="img/icons.svg#icon-man"></use>
                </svg>
                <span class="recipe__info-data recipe__info-data--people"> ${ recipe.servings } </span>
                <span class="recipe__info-text"> servings</span>

                <div class="recipe__info-buttons">
                    <button class="btn-tiny btn-decrease">
                        <svg>
                            <use href="img/icons.svg#icon-circle-with-minus"></use>
                        </svg>
                    </button>
                    <button class="btn-tiny btn-increase">
                        <svg>
                            <use href="img/icons.svg#icon-circle-with-plus"></use>
                        </svg>
                    </button>
                </div>

            </div>
            <button class="recipe__love">
                <svg class="header__likes">
                    <use href="img/icons.svg#icon-heart-outlined"></use>
                </svg>
            </button>
        </div>



        <div class="recipe__ingredients">
            <ul class="recipe__ingredient-list">
                
            </ul>

            <button class="btn-small recipe__btn">
                <svg class="search__icon">
                    <use href="img/icons.svg#icon-shopping-cart"></use>
                </svg>
                <span>Add to shopping list</span>
            </button>
        </div>

        <div class="recipe__directions">
            <h2 class="heading-2">How to cook it</h2>
            <p>
                ${ recipe.instructions }
            </p>

            <p class="recipe__directions-text">
                This recipe was carefully designed and tested by
                <span class="recipe__by">${ recipe.auther }</span>. Please check out directions at their website.
            </p>
            <a class="btn-small recipe__btn" href="${ recipe.source }" target="_blank">
                <span>Directions</span>
                <svg class="search__icon">
                    <use href="img/icons.svg#icon-triangle-right"></use>
                </svg>
            </a>
        </div>`;

        document.querySelector('.recipe').insertAdjacentHTML('afterbegin', template);
        randerIngredients(recipe.Ingredients);
}