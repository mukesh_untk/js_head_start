
export default

class Search {

    constructor (query) {
        this.query = query;
    }

    async getResults(offset) {
        const apikey= '1fbe300754eb4198b24d2155486a4b6a';
        let url = `https://api.spoonacular.com/recipes/search?query=${this.query}&apiKey=${apikey}&offset=${offset}&number=10`;
        const response = await fetch(url);
        this.result = await response.json();
    }
};