
export default

class Recipe {

    constructor (recipeId) {
        this.id = recipeId;
        this.Ingredients = [];
    }

    async getRecipe() {
        const apikey= '1fbe300754eb4198b24d2155486a4b6a';    
        let url = `https://api.spoonacular.com/recipes/${this.id}/information?includeNutrition=false&apiKey=${apikey}`;
        const response = await fetch(url);
        const result = await response.json();
        this.title = result.title;
        this.image = result.image;
        this.servings = result.servings;
        this.auther = result.sourceName;
        this.source = result.spoonacularSourceUrl;
        this.instructions = result.instructions;
        this.readyInMinutes = result.readyInMinutes;
        let counter = 1;
        result.extendedIngredients.forEach(el => {
            this.Ingredients.push({
                id : counter,
                name: el.originalName,
                amount: el.amount,
                unit: el.unit
            });
            counter++;
        });
        console.log(result);
    }
};