// UIController
var UIController = (function() {
    
    const DOMStrings = {
        ADD_BTN : '.add__btn',
        TYPE : '.add__type',
        DESC : '.add__description',
        VALUE : '.add__value',
        INCOME_LIST : '.income__list',
        EXPENSES_LIST : '.expenses__list',
        MONTH_LBL : '.budget__title--month',
        BUDGET_LBL : '.budget__value',
        INCOME_LBL : '.budget__income--value',
        EXPENSE_LBL : '.budget__expenses--value',
        PERCENTAGE_LBL : '.budget__expenses--percentage',
        CONTAINER : '.container'
    };

    function formatAmount (amt) {
        return amt.toFixed(2);
    }

    return {
        getDOMStrings : function() {
            return DOMStrings;
        },

        getInput : function() {
            return {
                type : document.querySelector(DOMStrings.TYPE).value,
                desc: document.querySelector(DOMStrings.DESC).value,
                val: parseFloat(document.querySelector(DOMStrings.VALUE).value)
            };
        },

        clearInput : function() {
            document.querySelector(DOMStrings.DESC).value = '';
            document.querySelector(DOMStrings.VALUE).value = '';
            document.querySelector(DOMStrings.TYPE).focus();
        },

        addItem : function(type, item) {
            
            var listWrapper = document.querySelector(DOMStrings.INCOME_LIST);
            var row = '<div class="item clearfix" id="inc-'+ item.ID + '"><div class="item__description">' + item.description + '</div><div class="right clearfix"><div class="item__value">+ ' + formatAmount(item.value) + '</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>';

            if (type === 'exp') {
                row = '<div class="item clearfix" id="exp-'+ item.ID + '"><div class="item__description">' + item.description + '</div><div class="right clearfix"><div class="item__value">- ' + formatAmount(item.value) + '</div><div class="item__percentage">---</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>';

                listWrapper = document.querySelector(DOMStrings.EXPENSES_LIST);
            }
            
            listWrapper.insertAdjacentHTML('afterbegin', row);
        },

        removeItem : function(id){
            var pa = document.getElementById(id).parentNode;
            pa.removeChild(document.getElementById(id));
        },

        updateBudget : function(obj) {
            document.querySelector(DOMStrings.BUDGET_LBL).textContent = formatAmount(obj.budget);
            document.querySelector(DOMStrings.INCOME_LBL).textContent = '+ ' + formatAmount(obj.inc);
            document.querySelector(DOMStrings.EXPENSE_LBL).textContent = '- ' + formatAmount(obj.exp);
            if (obj.percentage === -1) document.querySelector(DOMStrings.PERCENTAGE_LBL).textContent = '---';
            else document.querySelector(DOMStrings.PERCENTAGE_LBL).textContent = obj.percentage + '%';
        }
    }
})();

// BudgetController
var BudgetController = (function() {

    var Expense = function(id, des, val) {
        this.ID = id;
        this.description = des;
        this.value = val;
    }

    var Income = function(id, des, val) {
        this.ID = id;
        this.description = des;
        this.value = val;
    }

    function calculateTotal(type){
        var sum = 0;
        data.all[type].forEach(element => {
            sum += element.value;
        });
        data.total[type] = sum;
        return sum;
    }

    var data = {
        all: {
            exp : [],
            inc : []
        },
        total: {
            exp : 0,
            inc : 0
        },
        budget : 0,
        percentage : -1
    }

    return  {
        addItem : function (item) {
            var id = 0, Item;
            var count = data.all[item.type].length;
            if( count > 0 ) {
                id = data.all[item.type][count - 1].ID + 1;
            }
            if (item.type === 'exp') {
                Item = new Expense(id, item.desc, item.val);
            } else if (item.type === 'inc') {
                Item = new Income(id, item.desc, item.val);
            }
            data.all[item.type].push(Item);
            return Item;
        },

        removeItem : function(id) {
            var parts = id.split('-');
            var type = parts[0];
            var ID = parseInt(parts[1]);

            var ids = data.all[type].map(function(value, index, array){
                return value.ID;
            });
            console.log(ids);
            var idx = ids.indexOf(ID);
            data.all[type].splice(idx, 1);
        },

        updateBudget : function(){
            var e = calculateTotal('exp');
            var i = calculateTotal('inc');

            data.budget = data.total.inc - data.total.exp;
            if (data.total.inc>0)
                data.percentage = Math.round((data.total.exp / data.total.inc)*100);
            else
            data.percentage = -1;
            // return budget
            return {
                exp: e,
                inc: i,
                budget: data.budget,
                percentage: data.percentage
            }
        },

        test : function () {
            console.log(data);
        }
    };

})();

// AppController
var AppController = (function(UICtrl, BudgetCtrl) {

    function setupEventListeners() {
        var Dom = UICtrl.getDOMStrings();
        document.querySelector(Dom.ADD_BTN).addEventListener('click', addListItem);
        document.addEventListener('keypress', function(e) {
           if(e.keyCode === 13) {
               addListItem();
           }
        });
        document.querySelector(Dom.CONTAINER).addEventListener('click', function(event){
            var target = event.target;
            var id = target.parentNode.parentNode.parentNode.parentNode.id;
            if(id) {
                BudgetCtrl.removeItem(id);
                UICtrl.removeItem(id);
                updateBudget();
            }
        })
        //exception
        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'Septmber', 'October', 'November', 'December']
        var d = new Date();
        document.querySelector(Dom.MONTH_LBL).textContent = months[d.getMonth()];
    }

    function updateBudget () {
        // 1. Calculate the budget
        var budget = BudgetCtrl.updateBudget();
        console.log(budget);
        // 2. update ui
        UICtrl.updateBudget(budget);
    }

    function addListItem() {
        // 1. get the field input data
        var item = UICtrl.getInput();

        if ( item.desc !== '' && item.val && item.val !== 0) {
            // 2. add the item to the budget controller
            var newItem = BudgetCtrl.addItem(item);

            // 3. add the item to the ui
            UICtrl.addItem(item.type, newItem);

            // 4. clear fields
            UICtrl.clearInput();

            // 5. update the budget
            updateBudget();
        }
    }

    function init() {
        console.log('application started');
        UICtrl.updateBudget({
            exp: 0,
            inc: 0,
            budget: 0,
            percentage: -1
        });
        setupEventListeners();
    }

    return {
        init: init
    }
})(UIController, BudgetController);

AppController.init();